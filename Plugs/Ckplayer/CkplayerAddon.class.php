<?php
/**
 * 示列插件
 * @author PHP联盟
 */
namespace Addons\Ckplayer;
use Common\Controller\Addon;
class CkplayerAddon extends Addon
{
        public $info = array(
            'name'          =>  'Ckplayer',
            'title'         =>  '示列',
            'description'   =>  '这是一个插件描述！',
            'status'        =>  1,
            'author'        =>  'PHP联盟',
            'email'         =>  'Name_Cyu@Foxmail.com',
            'version'       =>  '0.1'
        );

        public $admin_list = array(
            'model'     =>  'Example',      // 要查的表
            'fields'    =>  '*',            // 要查的字段
            'map'       =>  '',             // 查询条件, 如果需要可以再插件类的构造方法里动态重置这个属性
            'order'     =>  'id desc',      // 排序,
            'listKey'   =>  array(
                // 这里定义的是除了id序号外的表格里字段显示的表头名
                '字段名'    => '表头显示名'
            ),
        );

        // 安装
        public function install()
        {
            return true;
        }

        // 卸载
        public function uninstall()
        {
            return true;
        }

        // 实现的test钩子方法
        public function test($param)
        {
            // 钩子行为方法
        }

}