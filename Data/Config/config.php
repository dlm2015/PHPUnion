<?php
return array(
	/***************默认设置*****************/
	'DEFAULT_TIMEZONE'      =>  'PRC',		// 默认时区
	'DEFAULT_LANG'          =>  'zh-cn', 	// 默认语言
	/***************模块相关*****************/
	'MULTI_MODULE'          =>  True,	// 是否允许多模块 如果为false 则必须设置 DEFAULT_MODULE
	'DEFAULT_MODULE'		=>	'Home',	//默认模块
	'MODULE_ALLOW_LIST'     =>    array('Home','Admin','Member','Install'),	// 设置允许访问模块
	/***************模板相关*****************/
	'VIEW_PATH'				=>  './',	//视图模版路径
	'TMPL_DETECT_THEME'     =>  True,	// 自动侦测模板主题
    'TMPL_DENY_PHP'         =>  false,	// 默认模板引擎是否禁用PHP原生代码
    'TMPL_CONTENT_TYPE'     =>  'text/html', // 默认模板输出类型
    'TMPL_ACTION_ERROR'     =>  'Theme/System/Error.html', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS'   =>  'Theme/System/Successs.html', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE'   =>  'Theme/System/exception.html',// 异常页面的模板文件
    'TMPL_PARSE_STRING' =>array(
    '__Static__' => __ROOT__.'/Lib',         // 替换默认的公共规则
    '__Bootstrap__' => __ROOT__.'/Lib/Bootstrap',         // 替换默认的公共规则
    '__Lib__' => __ROOT__.'/Lib',         // 替换默认的公共规则
    ),
    /***************路由相关设置*************/
    // 'USER_AUTH_ON' // 是否需要认证
    // 'USER_AUTH_TYPE' // 认证类型
    'USER_AUTH_KEY'         => 'AuthId', // 认证识别号
    // 'REQUIRE_AUTH_MODULE'  // 需要认证模块
    // 'NOT_AUTH_MODULE' // 无需认证模块
    // 'USER_AUTH_GATEWAY' // 认证网关
    // 'RBAC_DB_DSN'  // 数据库连接DSN
    // 'RBAC_ROLE_TABLE' // 角色表名称
    // 'RBAC_USER_TABLE' // 用户表名称
    // 'RBAC_ACCESS_TABLE' // 权限表名称
    // 'RBAC_NODE_TABLE' // 节点表名称

    /***************路由相关设置*************/
    'URL_ROUTER_ON'         =>  True,   // 是否开启URL路由
    'URL_CASE_INSENSITIVE'  =>  False,   // 表示URL区分大小写 true则表示不区分大小写
    'URL_MODEL'             =>  0,       // URL访问模式,可选参数0、1、2、3,代表以下四种模式：
    // 0 (普通模式); 1 (PATHINFO 模式); 2 (REWRITE  模式); 3 (兼容模式)  默认为PATHINFO 模式
    'URL_PATHINFO_DEPR'     =>  '/',	// PATHINFO模式下，各参数之间的分割符号
    'URL_PATHINFO_FETCH'    =>  'ORIG_PATH_INFO,REDIRECT_PATH_INFO,REDIRECT_URL', // 用于兼容判断PATH_INFO 参数的SERVER替代变量列表
    'URL_REQUEST_URI'       =>  'REQUEST_URI', // 获取当前页面地址的系统变量 默认为REQUEST_URI
    'URL_HTML_SUFFIX'       =>  'html',  // URL伪静态后缀设置
    'URL_DENY_SUFFIX'       =>  'ico|png|gif|jpg', // URL禁止访问的后缀设置
    'URL_PARAMS_BIND'       =>  true, // URL变量绑定到Action方法参数
    'URL_PARAMS_BIND_TYPE'  =>  0, // URL变量绑定的类型 0 按变量名绑定 1 按变量顺序绑定
    'URL_PARAMS_FILTER'     =>  false, // URL变量绑定过滤
    'URL_PARAMS_FILTER_TYPE'=>  '', // URL变量绑定过滤方法 如果为空 调用DEFAULT_FILTER
    'URL_ROUTE_RULES'       =>  array(), // 默认路由规则 针对模块
    /***************URL伪静态规则设置*****************/
    'URL_MAP_RULES'         =>  array(), // URL映射定义规则
);