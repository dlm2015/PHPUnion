/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : phpunion

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2015-06-02 11:18:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for php_access
-- ----------------------------
DROP TABLE IF EXISTS `php_access`;
CREATE TABLE `php_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_access
-- ----------------------------

-- ----------------------------
-- Table structure for php_cate
-- ----------------------------
DROP TABLE IF EXISTS `php_cate`;
CREATE TABLE `php_cate` (
  `cid` mediumint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '栏目ID',
  `pid` mediumint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父级ID',
  `catname` char(30) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `catdir` varchar(255) DEFAULT NULL,
  `cat_keyworks` varchar(255) DEFAULT '' COMMENT '栏目关键字',
  `cat_description` varchar(255) DEFAULT '' COMMENT '栏目描述',
  `index_tpl` varchar(200) DEFAULT '' COMMENT '封面模板',
  `list_tpl` varchar(200) DEFAULT '' COMMENT '列表页模板',
  `arc_tpl` varchar(200) DEFAULT '' COMMENT '内容页模板',
  `cat_html_url` varchar(200) DEFAULT '' COMMENT '栏目页URL规则\n\n',
  `arc_html_url` varchar(200) DEFAULT '' COMMENT '内容页URL规则',
  `cattype` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 栏目,2 封面,3 外部链接,4 单文章',
  `arc_url_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '文章访问方式 1 静态访问 2 动态访问',
  `cat_url_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '栏目访问方式 1 静态访问 2 动态访问',
  `cat_redirecturl` varchar(100) NOT NULL DEFAULT '' COMMENT '跳转url',
  `catorder` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目排序',
  `cat_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'channel标签调用时是否显示',
  `cat_seo_title` char(100) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `cat_seo_description` varchar(255) NOT NULL DEFAULT '' COMMENT 'SEO描述',
  `addtime` int(10) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='栏目表';

-- ----------------------------
-- Records of php_cate
-- ----------------------------
INSERT INTO `php_cate` VALUES ('1', '0', '微电影', 'movie', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '2', '2', '2', '', '0', '1', '', '', null);
INSERT INTO `php_cate` VALUES ('2', '0', '网络视频', 'work', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '2', '2', '2', '', '0', '1', '', '', null);
INSERT INTO `php_cate` VALUES ('4', '1', '励志微电影', 'lizhi', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '1', '2', '2', '', '0', '1', '', '', null);
INSERT INTO `php_cate` VALUES ('5', '2', '网络搞笑', 'wamgluo', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '1', '2', '2', '', '0', '1', '', '', null);

-- ----------------------------
-- Table structure for php_cate_access
-- ----------------------------
DROP TABLE IF EXISTS `php_cate_access`;
CREATE TABLE `php_cate_access` (
  `rid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目cid',
  `mid` smallint(1) NOT NULL DEFAULT '0' COMMENT '模型mid',
  `content` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许访问 1 允许 0 不允许',
  `add` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许投稿(添加) 1允许 0 不允许',
  `edit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许更新 1允许 0 不允许',
  `del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许删除 1允许 0 不允许',
  `order` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许排序 1允许 0 不允许',
  `move` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许移动 1允许 0 不允许',
  `audit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许审核栏目文章 1 允许 0 不允许',
  `admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为管理员权限 1 管理员 2 前台用户'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='栏目权限表';

-- ----------------------------
-- Records of php_cate_access
-- ----------------------------

-- ----------------------------
-- Table structure for php_config
-- ----------------------------
DROP TABLE IF EXISTS `php_config`;
CREATE TABLE `php_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cid` int(8) NOT NULL COMMENT '配置组ID',
  `name` varchar(50) DEFAULT NULL COMMENT '配置项变量名',
  `value` text COMMENT '配置值',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `type` enum('select','radio','text','textarea','group') DEFAULT 'text' COMMENT '类型',
  `info` text COMMENT '参数',
  `message` varchar(255) DEFAULT NULL COMMENT '描述提示',
  `status` tinyint(1) DEFAULT '1' COMMENT '1,正常 0禁用',
  `system` tinyint(1) DEFAULT NULL COMMENT '1系统,0普通',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_config
-- ----------------------------
INSERT INTO `php_config` VALUES ('1', '1', 'WEBNAME', '「PHP联盟」', '网站名称', 'text', '', '请输入网站名称！', '1', '1', '1431367267');
INSERT INTO `php_config` VALUES ('2', '1', 'DOMAIN', 'http://localhost/PHPUnion', '网站域名', 'text', '', '请输入本站网址，不要以 / 结尾', '1', '1', '1431367325');
INSERT INTO `php_config` VALUES ('3', '1', 'UPLOAD', 'upload', '附件目录', 'text', '', '请输入上传的附件目录！', '1', '1', '1431369313');
INSERT INTO `php_config` VALUES ('4', '1', 'SEO_TITLE', '致力开发开发专业视频管理系统,视频上传转码,视频云储存', 'SEO标题', 'text', '', '请输入副的SEO标题！', '1', '1', '1432095422');
INSERT INTO `php_config` VALUES ('5', '1', 'WEB_OPEN', '1', '网站开关', 'radio', '1|开启,0|关闭', '请选择网站是否开启', '1', '1', '1432095612');
INSERT INTO `php_config` VALUES ('6', '5', 'WEB_STYLE', '7eplayer', '模板设置', 'text', '', '选择模板的配置项！', '1', '1', '1432095933');
INSERT INTO `php_config` VALUES ('7', '1', 'WEB_CLOSE_MESSAGE', '网站正在维护中，请稍后访问！感谢您的支持。', '网站提示', 'textarea', '', '请输入网站关闭提示信息！', '1', '1', '1432096147');
INSERT INTO `php_config` VALUES ('8', '1', 'EMAIL', 'Name_Cyu@Foxmail.com', '邮箱联系', 'text', '', '请输入站长邮箱！', '1', '1', '1432096798');
INSERT INTO `php_config` VALUES ('9', '1', 'ADDRESS', '贵州省安顺市西秀区南水二路', '联系地址', 'text', '', '请输入现实联系地址！', '1', '1', '1432096880');
INSERT INTO `php_config` VALUES ('10', '1', 'MOBILE', '18788654245', '联系电话', 'text', '', '请输入联系电话号码！', '1', '1', '1432096935');
INSERT INTO `php_config` VALUES ('14', '3', 'SHOW_PAGE_TRACE', '0', '开发者模式', 'radio', '1|开启,0|关闭', '请选择是否开启开发者页面调试工具！', '1', '1', '1432440582');

-- ----------------------------
-- Table structure for php_config_group
-- ----------------------------
DROP TABLE IF EXISTS `php_config_group`;
CREATE TABLE `php_config_group` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置组ID',
  `cname` varchar(100) DEFAULT NULL COMMENT '配置组标识',
  `ctitle` varchar(100) DEFAULT NULL COMMENT '配置组名称',
  `csort` int(8) DEFAULT '0' COMMENT '排序',
  `isshow` tinyint(1) DEFAULT NULL COMMENT '1显示,0隐藏',
  `system` tinyint(1) DEFAULT '0' COMMENT '1系统组 0普通组',
  `select` tinyint(1) DEFAULT '0' COMMENT '标签默认选择',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_config_group
-- ----------------------------
INSERT INTO `php_config_group` VALUES ('1', 'system', '基本设置', '0', '1', '1', '1', '1431367065');
INSERT INTO `php_config_group` VALUES ('2', 'Member', '会员中心', '0', '1', '1', '0', '1431367088');
INSERT INTO `php_config_group` VALUES ('3', 'Performance', '性能设置', '0', '1', '1', '0', '1431367128');
INSERT INTO `php_config_group` VALUES ('4', 'Email', '邮箱设置', '0', '1', '1', '0', '1431367151');
INSERT INTO `php_config_group` VALUES ('5', 'Theme', '模板管理', '0', '0', '1', '0', '1431367166');
INSERT INTO `php_config_group` VALUES ('6', 'Images', '图组设置', '0', '1', '0', '0', '1431369269');
INSERT INTO `php_config_group` VALUES ('8', 'Videoinfo', '视频参数', '0', '1', '0', '0', null);

-- ----------------------------
-- Table structure for php_credits
-- ----------------------------
DROP TABLE IF EXISTS `php_credits`;
CREATE TABLE `php_credits` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `credits` int(11) DEFAULT NULL COMMENT '积分小于',
  `comment_state` tinyint(1) DEFAULT '1' COMMENT '评论审核 1审核，0未审核',
  `allowsendmessage` tinyint(1) DEFAULT '1' COMMENT '短消息 1允许 0不允许',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_credits
-- ----------------------------

-- ----------------------------
-- Table structure for php_flag
-- ----------------------------
DROP TABLE IF EXISTS `php_flag`;
CREATE TABLE `php_flag` (
  `fid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_flag
-- ----------------------------
INSERT INTO `php_flag` VALUES ('1', '热门', null);
INSERT INTO `php_flag` VALUES ('2', '置顶', null);
INSERT INTO `php_flag` VALUES ('3', '推荐', null);
INSERT INTO `php_flag` VALUES ('4', '图片', null);
INSERT INTO `php_flag` VALUES ('5', '精华', null);
INSERT INTO `php_flag` VALUES ('6', '幻灯片', null);
INSERT INTO `php_flag` VALUES ('7', '站长推荐', null);

-- ----------------------------
-- Table structure for php_group
-- ----------------------------
DROP TABLE IF EXISTS `php_group`;
CREATE TABLE `php_group` (
  `gid` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `gname` varchar(20) NOT NULL COMMENT '用户组名称',
  `pid` smallint(6) DEFAULT '2' COMMENT '1|系统组,2|会员组',
  `status` tinyint(1) unsigned DEFAULT NULL COMMENT '1|开启,0|禁用',
  `gremark` varchar(255) DEFAULT NULL COMMENT '描述',
  `addtime` int(11) DEFAULT NULL COMMENT '1系统组 0普通组',
  PRIMARY KEY (`gid`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_group
-- ----------------------------
INSERT INTO `php_group` VALUES ('4', '游客', '2', '1', 'fff', null);
INSERT INTO `php_group` VALUES ('7', '普通会员', '2', '1', 'ffggfdg', null);
INSERT INTO `php_group` VALUES ('8', 'name', '2', '1', null, null);
INSERT INTO `php_group` VALUES ('9', '123456', '2', '1', 'aaa', null);
INSERT INTO `php_group` VALUES ('10', 'test', '2', '1', '123456', null);
INSERT INTO `php_group` VALUES ('11', 'dsadsa', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('12', 'dsadsa', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('13', 'dsadasda', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('14', 'ghffdg', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('15', '1234566', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('16', '普通会员', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('17', '普通会员', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('18', '普通会员', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('19', '普通会员', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('20', '普通会员', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('21', '普通会员', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('22', '普通会员1111', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('23', '958416459', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('24', 'adsadsa', '2', '1', '', null);
INSERT INTO `php_group` VALUES ('25', '123456', '2', '1', '', null);

-- ----------------------------
-- Table structure for php_hooks
-- ----------------------------
DROP TABLE IF EXISTS `php_hooks`;
CREATE TABLE `php_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型 1控制器 0视图',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_hooks
-- ----------------------------
INSERT INTO `php_hooks` VALUES ('4', 'app_begin', '应用开始', '1', '1433179553');

-- ----------------------------
-- Table structure for php_member
-- ----------------------------
DROP TABLE IF EXISTS `php_member`;
CREATE TABLE `php_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `gid` int(11) DEFAULT NULL COMMENT '会员组ID',
  `username` char(30) NOT NULL DEFAULT '',
  `nickname` char(30) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` char(40) NOT NULL DEFAULT '',
  `code` char(30) NOT NULL DEFAULT '' COMMENT '密码key',
  `user_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1  正常  0 锁定',
  `lock_time` char(20) NOT NULL COMMENT '锁定到期时间',
  `qq` char(20) NOT NULL DEFAULT '' COMMENT 'qq号码',
  `email` char(30) NOT NULL DEFAULT '' COMMENT '邮箱',
  `moble` bigint(20) DEFAULT NULL COMMENT '手机号码',
  `sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 男 2 女 3 保密',
  `credits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `sign` varchar(255) NOT NULL DEFAULT '' COMMENT '个性签名',
  `spec_num` mediumint(9) unsigned NOT NULL DEFAULT '0' COMMENT '空间访问数',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `regip` char(20) NOT NULL DEFAULT '' COMMENT '注册IP',
  `lastip` char(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `regtime` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
-- Records of php_member
-- ----------------------------
INSERT INTO `php_member` VALUES ('3', '7', null, 'chuyou100235', '55', '313b225cbe32a6b5a5b6a9f64e0dd859', 'd65190f528', '1', '0', '', '9584164599999@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1431793006', '1430322104');
INSERT INTO `php_member` VALUES ('4', '7', null, 'cyu10023', 'cyu10023', 'd041cc8428e0a92e0793aea94cd3c321', '1ed9fadcf6', '1', '0', '', '747009804@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430324166', '1430324166');
INSERT INTO `php_member` VALUES ('6', '7', null, 'fdsfds', 'fdsf', 'dd4ee8fedf717caa6e609bb8a1164f0f', 'bdf037f32b', '1', '0', '', '958416459@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430324747', '1430324747');
INSERT INTO `php_member` VALUES ('7', '7', null, 'test', 'test', '5c19db746dab64922cc23aaf495b0110', '8d4e7cc4ff', '1', '0', '', '123456@qq.com', null, '2', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430324879', '1430324879');
INSERT INTO `php_member` VALUES ('8', '4', null, 'test123', '123456', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325636', '1430325636');
INSERT INTO `php_member` VALUES ('9', '4', null, '12345', '123456', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325677', '1430325677');
INSERT INTO `php_member` VALUES ('10', '4', null, '666', '66', 'e9510081ac30ffa83f10b68cde1cac07', '', '1', '2015', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325745', '1430325745');
INSERT INTO `php_member` VALUES ('11', '4', null, '6666', '66', '21232f297a57a5a743894a0e4a801fc3', '', '1', '2015-04-30 12:43:30', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325818', '1430325818');
INSERT INTO `php_member` VALUES ('12', '4', null, 'aaaa', '', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015/04/30 12:52:22', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430326349', '1430326349');
INSERT INTO `php_member` VALUES ('13', '4', null, 'tianfeng', 'tianfeng', '53ab5f415825c5368603caa68d35e1e3', '', '1', '', '', '', null, '1', '100', '', '0', '', '0.0.0.0', '0.0.0.0', '0', '1430339344');
INSERT INTO `php_member` VALUES ('14', '4', null, 'test1', '测试管理员', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '', '', '958416459@qq.com', null, '1', '0', '', '0', '', '0.0.0.0', '0.0.0.0', '1430339550', '1430339550');
INSERT INTO `php_member` VALUES ('15', '4', null, 'ggfgdfg', 'aa', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-02 08:18:45', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430569132', '1430569132');
INSERT INTO `php_member` VALUES ('16', '4', null, '9584164599', '958416459', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-02 08:19:05', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430569154', '1430569154');
INSERT INTO `php_member` VALUES ('17', '7', null, 'cyu100', '战神', '5e3a5ce533d9f3173a5d948021ed0e55', '8c922a5c71', '1', '', '', '1113581489@qq.com', null, '2', '0', '', '0', '', '0.0.0.0', '0.0.0.0', '1430675489', '1430675489');
INSERT INTO `php_member` VALUES ('18', '4', null, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '1', '', '', '', null, '1', '0', '', '0', '', '0.0.0.0', '0.0.0.0', '1432118451', '1431793029');
INSERT INTO `php_member` VALUES ('19', '4', null, 'username', 'username', '21232f297a57a5a743894a0e4a801fc3', '', '1', '2015-05-20 07:00:07', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432119616', '1432119616');
INSERT INTO `php_member` VALUES ('20', '4', null, '958416459', '958416459', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:32:55', '', '958416459qq@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460035', '1432460035');
INSERT INTO `php_member` VALUES ('21', '4', null, 'fdsfdsfds', 'fdsfsd', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:34:47', '', '9558445656@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460098', '1432460098');
INSERT INTO `php_member` VALUES ('22', '4', null, '123456', '123456', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:35:04', '', '958416459@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460118', '1432460118');
INSERT INTO `php_member` VALUES ('23', '7', null, '958416459999', '958416459', '91ba212414c6763d8d94d962141bc447', 'efc003b469', '1', '2015-05-24 17:39:09', '', '958416459@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460361', '1432460361');
INSERT INTO `php_member` VALUES ('24', '4', null, '55555', '51651651', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:40:30', '', '958416459@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460437', '1432460437');
INSERT INTO `php_member` VALUES ('25', '4', null, '95841645999999', '99999', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:43:41', '', '958416459@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460629', '1432460629');
INSERT INTO `php_member` VALUES ('26', '4', null, 'dsadsadsadsa', 'ddsadsad', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:45:19', '', '958416459@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460731', '1432460731');
INSERT INTO `php_member` VALUES ('27', '4', null, '95841645999999231', '156156', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:46:43', '', '958416459@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432460810', '1432460810');
INSERT INTO `php_member` VALUES ('28', '4', null, '1651561', '95841', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:53:11', '', '989594@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461275', '1432461275');
INSERT INTO `php_member` VALUES ('29', '4', null, '123456466', '958416459', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 17:58:06', '', '1234566515@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461501', '1432461501');
INSERT INTO `php_member` VALUES ('30', '4', null, '6666666', '9584', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 18:01:42', '', '123456@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461718', '1432461718');
INSERT INTO `php_member` VALUES ('31', '4', null, '316516', '561561', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 18:02:29', '', '515616165@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461760', '1432461760');
INSERT INTO `php_member` VALUES ('32', '4', null, '1321561565', '958416459@qq.com', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '2015-05-24 18:04:16', '', '65156@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461870', '1432461870');
INSERT INTO `php_member` VALUES ('33', '4', null, '1321561565aa', '958416459@qq.com', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-24 18:04:53', '', '65156@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461902', '1432461902');
INSERT INTO `php_member` VALUES ('34', '4', null, '1321561565aaf', '958416459@qq.com', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '2015-05-24 18:05:12', '', '65156@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461920', '1432461920');
INSERT INTO `php_member` VALUES ('35', '4', null, '1321561565aafdd', '958416459@qq.com', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '2015-05-24 18:05:55', '', '65156@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432461963', '1432461963');
INSERT INTO `php_member` VALUES ('36', '4', null, '165156', '516651', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '2015-05-24 18:06:38', '', '5165165@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432462009', '1432462009');
INSERT INTO `php_member` VALUES ('37', '4', null, '66516556', '651561', '95547383b23c86e324b395ebd52796d9', '284120493f', '1', '2015-05-24 18:28:24', '', '561561156@qq.com', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432463324', '1432463324');

-- ----------------------------
-- Table structure for php_node
-- ----------------------------
DROP TABLE IF EXISTS `php_node`;
CREATE TABLE `php_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_node
-- ----------------------------

-- ----------------------------
-- Table structure for php_plugs
-- ----------------------------
DROP TABLE IF EXISTS `php_plugs`;
CREATE TABLE `php_plugs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `email` varchar(50) DEFAULT '' COMMENT '作者邮箱',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of php_plugs
-- ----------------------------

-- ----------------------------
-- Table structure for php_role
-- ----------------------------
DROP TABLE IF EXISTS `php_role`;
CREATE TABLE `php_role` (
  `rid` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rname` varchar(20) NOT NULL COMMENT '用户组名称',
  `pid` smallint(6) DEFAULT '2' COMMENT '1|系统组,2|会员组',
  `status` tinyint(1) unsigned DEFAULT '2' COMMENT '1|系统组,2普通组',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `addtime` int(11) DEFAULT NULL COMMENT '1系统组 0普通组',
  PRIMARY KEY (`rid`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_role
-- ----------------------------
INSERT INTO `php_role` VALUES ('4', '游客', '2', '1', '', null);
INSERT INTO `php_role` VALUES ('7', '普通会员', '2', '1', '', null);

-- ----------------------------
-- Table structure for php_role_user
-- ----------------------------
DROP TABLE IF EXISTS `php_role_user`;
CREATE TABLE `php_role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(9) unsigned DEFAULT '0' COMMENT '用户权限组ID',
  `user_id` int(32) DEFAULT '0' COMMENT '会员ID',
  `group_id` int(10) DEFAULT '0' COMMENT '会员组ID',
  `credits_id` int(11) DEFAULT NULL COMMENT '积分id',
  PRIMARY KEY (`id`),
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_role_user
-- ----------------------------

-- ----------------------------
-- Table structure for php_upload
-- ----------------------------
DROP TABLE IF EXISTS `php_upload`;
CREATE TABLE `php_upload` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户uid',
  `name` varchar(255) DEFAULT '' COMMENT '原文件名',
  `filename` varchar(100) NOT NULL DEFAULT '' COMMENT '文件名',
  `basename` varchar(100) NOT NULL DEFAULT '' COMMENT '有扩展名的文件名',
  `path` char(200) NOT NULL DEFAULT '' COMMENT '文件路径 ',
  `ext` varchar(45) NOT NULL DEFAULT '' COMMENT '扩展名',
  `image` tinyint(1) NOT NULL DEFAULT '1' COMMENT '图片',
  `size` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否使用 1 使用 0 未使用',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `basename` (`basename`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='上传文件';

-- ----------------------------
-- Records of php_upload
-- ----------------------------
INSERT INTO `php_upload` VALUES ('1', '3', 'aaa', 'dsdsadsa', 'dsadasd', 'ddsa', '', '1', '561651', '0', null);

-- ----------------------------
-- Table structure for php_video
-- ----------------------------
DROP TABLE IF EXISTS `php_video`;
CREATE TABLE `php_video` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目cid',
  `uid` int(10) unsigned NOT NULL COMMENT '用户uid',
  `fid` int(5) DEFAULT NULL COMMENT '推荐位id',
  `title` char(100) NOT NULL DEFAULT '' COMMENT '标题',
  `tag` char(255) DEFAULT NULL COMMENT '标签云',
  `new_window` tinyint(1) NOT NULL DEFAULT '0' COMMENT '新窗口打开 0否 , 1是',
  `seo_title` char(100) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图',
  `video_path` char(255) DEFAULT NULL COMMENT '视频路径',
  `content` varchar(255) DEFAULT '' COMMENT '视频简介',
  `click` int(6) NOT NULL DEFAULT '0' COMMENT '点击数',
  `url_type` tinyint(80) NOT NULL DEFAULT '3' COMMENT '访问方式  1 静态访问  2 动态访问  3 继承栏目',
  `arc_sort` mediumint(6) NOT NULL DEFAULT '0' COMMENT '排序',
  `video_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态  0 未审核 1 已审核',
  `keywords` varchar(100) DEFAULT '' COMMENT '关键字',
  `updatetime` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addtime` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`aid`),
  KEY `uid` (`uid`),
  KEY `cid` (`cid`),
  KEY `content_status` (`video_status`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of php_video
-- ----------------------------
INSERT INTO `php_video` VALUES ('4', '4', '3', null, '那一年的春夏！微电影', '', '1', '', '', null, 'dsad', '0', '1', '0', '1', '', '1432126441', '1431628058');
INSERT INTO `php_video` VALUES ('6', '4', '3', null, '田埂上的梦！', '', '1', '', '', null, 'dsadas', '0', '3', '0', '1', '', '1432126585', '1432123729');
INSERT INTO `php_video` VALUES ('7', '5', '18', null, '958416459', '', '1', '', '', null, '5665', '0', '3', '0', '1', '', '0', '2015');
