<?php
/**
 * 「PHP联盟」
 * 入口文件
 * @author：楚羽幽
 */

// 判断PHP版本
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('PHP版本必须是 5.3.0 以上 ，「PHP联盟」提示!');

// 框架路径
define('UNION_PATH','./Union/');

// 调试模式
define ( 'APP_DEBUG',TRUE);

// 应用名称
define ( 'APP_NAME','Home');

// 应用目录
define ( 'APP_PATH','./Home/');

// 缓存目录
define ( 'RUNTIME_PATH','./Cache/');

// 引用框架文件
require_once UNION_PATH.'Union.php';