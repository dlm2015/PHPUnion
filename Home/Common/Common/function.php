<?php
/**
 *	函数自定义
 *	@author 楚羽幽
 *	QQ 958416459
 */

/**
 * 定义变量
 */
const UNION_ADDON_PATH = './Plugs/';

/**
 *  打印数组函数
 *  @author 楚羽幽
 */
function p($arr)
{
	echo '<pre>';
	print_r($arr);
	echo '</pre>';
}

/**
 * 根据大小返回标准单位 KB  MB GB等
 */
function get_size($size, $decimals = 2)
{
    switch (true)
    {
        case $size >= pow(1024, 3):
            return round($size / pow(1024, 3), $decimals) . " GB";
        case $size >= pow(1024, 2):
            return round($size / pow(1024, 2), $decimals) . " MB";
        case $size >= pow(1024, 1):
            return round($size / pow(1024, 1), $decimals) . " KB";
        default:
            return $size . 'B';
    }
}

/**
 * [create_dir_or_files 基于数组创建目录和文件]
 * @param  [type] $files [description]
 * @return [type]        [description]
 */
function create_dir_or_files($files)
{
    foreach ($files as $key => $value)
    {
        if(substr($value, -1) == '/')
        {
            mkdir($value);
        }
        else
        {
            @file_put_contents($value, '');
        }
    }
}

/**
 * 数组进行整数映射转换
 * @param       $data
 * @param array $map
 */
function int_to_string(&$data, array $map = array('status' => array('0' => '禁止', '1' => '启用')))
{
    $map = (array)$map;
    foreach ($data as $n => $d)
    {
        foreach ($map as $name => $m)
        {
            if (isset($d[$name]) && isset($m[$d[$name]]))
            {
                $data[$n][$name . '_text'] = $m[$d[$name]];
            }
        }
    }
}

/**
 * 获取插件类的类名
 * @param strng $name 插件名
 */
function get_addon_class($name)
{
    $class = "Plugs\\{$name}\\{$name}Addon";
    return $class;
}


/**
* 对查询结果集进行排序
* @access public
* @param array $list 查询结果
* @param string $field 排序的字段名
* @param array $sortby 排序类型
* asc正向排序 desc逆向排序 nat自然排序
* @return array
*/
function list_sort_by($list,$field, $sortby = 'asc')
{
   if(is_array($list))
   {
       $refer = $resultSet = array();
       foreach ($list as $i => $data)
           $refer[$i] = &$data[$field];
       switch ($sortby)
       {
           case 'asc': // 正向排序
                asort($refer);
                break;
           case 'desc':// 逆向排序
                arsort($refer);
                break;
           case 'nat': // 自然排序
                natcasesort($refer);
                break;
       }
       foreach ( $refer as $key=> $val)
           $resultSet[] = &$list[$key];
       return $resultSet;
   }
   return false;
}