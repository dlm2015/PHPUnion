<?php
$result = array(
	/***************模板相关*****************/
	'TMPL_L_DELIM'          =>  '{?',	// 模板引擎普通标签开始标记
    'TMPL_R_DELIM'          =>  '?}',	// 模板引擎普通标签结束标记
    'TMPL_DENY_PHP'         =>  false,	// 默认模板引擎是否禁用PHP原生代码
    'TMPL_CONTENT_TYPE'     =>  'text/html', // 默认模板输出类型
	/*--------------------------第三方类库-----------------------------------*/
	'AUTOLOAD_NAMESPACE' => array(
		'Lib' => APP_PATH.'Lib',
	),
	/***************程序信息*****************/
    'SYSTEM_WEBNAME'		=>  '「PHP联盟」视频管理系统',
    'SYSTEM_AUTHOR'			=>  '楚羽幽',
    'SYSTEM_VERSION'		=>  'Beta V0.1',
    'SYSTEM_VERSION_TIME'	=>  '2015-5-23',
    'SYSTEM_EMAIL'          =>  'Name_Cyu@Foxmail.Com',
    'SYSTEM_QQ'             =>  '1113581489',
    'SYSTEM_FLOCK'			=>  '383186297',
    'SYSTEM_DOMAIN'			=>  'http://www.PHPunion.cn',
	/*--------------------------插件系统机制-----------------------------------*/
	'ADDON_PATH'			=>	'./Plugs',
	/*--------------------------自定义标签库-----------------------------------*/
	'APP_AUTOLOAD_PATH'		=> '@.Taglib',
	'TAGLIB_BUILD_IN'		=> 'Cx,Lib\TagLib\Cy',
	'TAGLIB_PRE_LOAD'		=> 'Cx,Lib\TagLib\Cy',
	);
return array_merge($result,require 'Data/Config/config.inc.php',require 'Data/Config/db.config.php',require 'Data/Config/config.php');