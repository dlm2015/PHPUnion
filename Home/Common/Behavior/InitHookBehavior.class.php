<?php
/**
 * 「PHP联盟」 - 运行插件行为
 * 应用开始
 * 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Common\Behavior;
use Think\Behavior;
use Think\Hook;
defined('THINK_PATH') or exit();
// 初始化钩子信息
class InitHookBehavior extends Behavior
{

    // 行为扩展的执行入口必须是run
    public function run(&$content)
    {
        // 检测是否安装程序
        if(isset($_GET['m']) && $_GET['m'] === 'Install') return;
        
        $data = S('hooks');
        if(!$data)
        {
            $hooks = M('Hooks')->getField('name');
            foreach ($hooks as $key => $value)
            {
                if($value)
                {
                    $map['status']  =   1;
                    $names          =   explode(',',$value);
                    $map['name']    =   array('IN',$names);
                    $data = M('Plugs')->where($map)->getField('id,name');
                    if($data)
                    {
                        $addons = array_intersect($names, $data);
                        Hook::add($key,$addons);
                    }
                }
            }
            S('hooks',Hook::get());
        }
        else
        {
            Hook::import($data,false);
        }
    }
}