<?php
/**
 * 附件表与用户关联模型
 * Class UploadModel
 * @author 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Model;
use Think\Model\RelationModel;
class UploadModel extends RelationModel
{
    protected $tableName = 'Upload';
    protected $_link = array(
        'User'  =>  array(
            'mapping_type'  => self::HAS_ONE,
            'class_name'    => 'User',
            'foreign_key'   => 'uid',
            'mapping_key'   => 'uid',
        ),
    );

    /**
     * 删除文件
     * @param $id
     */
    public function delFile($id)
    {
        $db = M('upload');
        $data = $db->find($id);
        if ($data)
        {
            is_file($data['path']) && @unlink($data['path']);
            return $db->delete($id);
        }
    }
}
