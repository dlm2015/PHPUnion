<?php
/**
 * 插件模型
 * @author yangweijie <yangweijiester@gmail.com>
 */
namespace Admin\Model;
use Think\Model;
class PlugsModel extends Model
{
    // 模型主表
    protected $tableName = 'Plugs';
    /**
     * [$_validate 自动验证]
     * @var array
     */
    protected $_validate = array(
        // array(验证字段1,验证规则,错误提示,[验证条件,附加规则,验证时间]),
        array('name','require','插件标识必须填写！',1,'',3),
        array('name','/^[a-zA-Z]+$/i','插件标识必须为英文字母',1,'regex',3),
        array('title', 'require', '插件名称必须填写！', 1,'', 3),
        array('version', 'require', '插件版本必须填写！', 1,'', 3),
        array('email', 'require', '作者邮箱必须填写！', 1,'', 3),
        array('author', 'require', '插件作者必须填写！', 1,'', 3),
        array('description', 'require', '插件描述必须填写！', 1,'', 3),
    );


    /**
     * [$_auto 自动完成]
     * @var array
     */
    protected $_auto = array(
    );

    /**
     * 获取插件列表
     * @param string $addon_dir
     */
    public function getList($addon_dir = '')
    {
        //取得模块目录名称
        $dirs = array_map('basename', glob(UNION_ADDON_PATH . '*', GLOB_ONLYDIR));
        if (empty($dirs))
        {
            return false;
        }

        $addons = array();
        $addonList = $this->where(array('name' => array('IN', $dirs)))->order('id ASC')->select();
        if ($addonList)
        {
            foreach ($addonList as $addon)
            {
                $addon['install'] = 1;
                $addon['config'] = unserialize($addon['config']);
                $addons[$addon['name']] = $addon;
            }
        }
        foreach ($dirs as $d)
        {
            $addonObj = $this->getAddonObj($d); //获得插件对象
            if (!$addonObj) continue;

            //没有安装插件
            if (!isset($addons[$d]))
            {
                $addon = $addonObj->info;
                $addon['status']=0;
                $addon['install'] = 0;
                $addon['config'] = $addonObj->getConfig(); //获得插件配置
                $addons[$d] = $addon;
            }
            else
            {
                //前台文件
                $indexActionFile = UNION_ADDON_PATH . $d . '/Controller/IndexController.class.php';
                $addons[$d]['IndexAction'] = is_file($indexActionFile) ? __WEB__."?g=Addon&m={$d}&c=Index&a=index": '';
            }

            //插件帮助文档
            $addons[$d]['help'] = is_file(UNION_ADDON_PATH . $d . '/help.html') ? U('help') . '&addon=' . $d : '';
        }
        int_to_string($addons, array('status' => array(1 => '启用', 0 => '禁用')));
        ksort($addons);
        return $addons;
    }


    /**
     * 获得插件对象
     * @param $addon 插件名
     * @return mixed;
     */
    public function getAddonObj($addon)
    {
        $classFile = UNION_ADDON_PATH . $addon . '/' . $addon . 'Addon.class.php';
        if (!is_file($classFile)) return false;
        require_cache($classFile);
        $class = $addon . 'Addon'; //类名
        if (!class_exists($class))
        {
            return false;
        }
        return new $class;
    }
    /*------------------------------------------属性定义--------------------------------------------------*/

    /**
     * [getAdminList 获取已安装插件，并有后台面板插件]
     * @return [type] [description]
     */
    public function getAdminList()
    {
        $admin = array();
        $db_addons = $this->where("status=1 AND has_adminlist=1")->field('title,name')->select();
        if($db_addons)
        {
            foreach ($db_addons as $value)
            {
                $admin[] = array('title'=>$value['title'],'url'=>"Addons/adminList?name={$value['name']}");
            }
        }
        return $admin;
    }
}
