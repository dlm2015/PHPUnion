<?php
/**
 * 用户管理关联模型
 * Class UserModel
 */
namespace Admin\Model;
use Think\Model\RelationModel;
class MemberRelationModel extends RelationModel
{
    protected $tableName = 'Member';
    protected $_link = array(
        'Role' => array(
            'mapping_type'  => self::MANY_TO_MANY, // 多对多关系
            'relation_table'=> 'php_role_user', // 多对多的中间关联表名称
            'foreign_key'   => 'user_id', // 关联的外键名称 外键的默认规则是当前数据对象名称_id
            'relation_foreign_key'=> 'role_id',
            'mapping_fields'=> 'rname', // 关联要查询的字段
            ),
        );

    /**
     * [$auto 自动完成]
     * @var array
     */
    public $_auto = array(
        // array('完成字段','处理方法及函数','处理时机','方法及函数类型'),
        array('credits', 'intval', 3, 'function'), // 积分转整型
        array('password', 'md5', 3, 'function'), // 密码自动加密
        array('regtime', 'time', 1, 'function'), // 注册时间
        array('logintime', 'time', 1, 'function'), // 登录时间初始化
        array('regip', 'get_client_ip', 1, 'function'), // 注册IP
        array('lastip', 'get_client_ip', 1, 'function'), // 登录IP初始化
    );
    /**
     * [$_validate 自动验证]
     * @var array
     */
    public $_validate = array(
        // array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间),
        array('username', 'require', '用户名不能为空', 1, '', 3),
        array('nickname', 'require', '昵称不能为空！', 1, '', 3),
        array('password', 'require', '密码不能为空！', 1, '', 1),
        array('password_c', 'password', '密码不一致！', 0, 'confirm', 3),
        array('email', 'email', '邮箱格式不正确', 0,'', 3),
        array('email', 'IsEmail', '邮箱已经存在', 1,'callback', 3),
    );

    /**
     * [addUser 添加帐号]
     */
    public function addUser()
    {
        if ($this->create())
        {
            $map['username'] = array('EQ', $this->data['username']);
            // 用户名检测
            if (M('user')->where($map)->find())
            {
                $this->error = '「PHP联盟」温馨提示：用户名已经存在！';
                return false;
            }

            // 获取密码Key
            $code = $this->getUserCode();

            // 数组重组
            $this->data['code'] = $code;
            $this->data['password'] = md5($this->data['password'] . $this->data['code']);

            if ($uid = $this->add())
            {
                $data = array(
                    'user_id'   => $uid,
                    'role_id'   => $_POST['rid'],
                );
                return M('role_user')->add($data);
            }
            else
            {
                $this->error = '用户添加失败';
                return false;
            }
        }
        else
        {
            $this->error = '无法创建Create数据！';
        }
    }


    /**
     * 修改用户
     */
    public function editUser()
    {
        $uid = I('uid', 0, 'intval');
        if ($this->create())
        {
            // 没有添加密码时删除密码数据
            if (empty($_POST['password']))
            {
                unset($_POST['password']);
                unset($this->data['password']);

                unset($_POST['password_c']);
                unset($this->data['password_c']);
            }

            // 获取密码Key
            $code = $this->getUserCode();
            
            // 数组重组
            $this->data['code'] = $code;
            $this->data['password'] = md5($this->data['password']. $code);

            $uid = $_POST['uid'];
            $rid = $_POST['rid'];
            // p($this->data);exit;            
            // 更新数据处理
            if ($this->where(array('uid'=> $uid))->save())
            {
                M('role_user')->where(array('user_id'=> $uid))->setField('role_id',$rid);
                return true;
            }
            else
            {
                $this->error = '用户信息修改失败！';
            }
            
        }
    }


    /**
     * 删除用户
     * @return mixed
     */
    public function delUser()
    {
        $uid = I('uid', 0, 'intval');
        // 删除视频
        if (I('post.delcontent'))
        {
            $map = array(
                'uid' => array('EQ', $uid)
            );
            $ModelCache = S('model');
            foreach ($ModelCache as $model)
            {
                $contentModel = ContentModel::getInstance($model['mid']);
                $contentModel->where($map)->del();
            }
        }

        //评论表存在时删除评论
        if (I('post.delcomment') && M()->tableExists('addon_comment')) {
            $map = array(
                'userid' => array('EQ', $uid)
            );
            M('addon_comment')->where($map)->del();
        }

        //删除附件
        if (I('post.delupload')) {
            $map = array(
                'uid' => array('EQ', $uid)
            );
            M('upload')->where($map)->del();
        }

        //删除访客
        M('user_guest')->where($map)->del();

        //日志记录
        $map = array(
            'uid' => array('EQ', $uid)
        );
        
        M('user_credits')->where($map)->del();
        if (M('user')->del($uid))
        {
            return true;
        }
        $this->error = '删除失败';
    }


    /*----------------------------------属性定义---------------------------------------*/

    /**
     * [IsEmail 邮箱验证]
     * @param [type] $name  [description]
     * @param [type] $value [description]
     * @param [type] $msg   [description]
     * @param [type] $arg   [description]
     */
    public function IsEmail($name, $value, $msg, $arg)
    {
        $uid = I('uid', 0, 'intval');
        if ($uid)
        {
            $map['uid'] = array('NEQ', $uid);
        }
        $map['email'] = array('EQ', $value);
        if (M('user')->where($map)->find())
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    /**
     * 获取用户密码加密key
     * @return string
     */
    public function getUserCode()
    {
        return substr(md5(C("USER_AUTH_KEY") . mt_rand(1, 1000) . time() . C('USER_AUTH_KEY')), 0, 10);
    }
}