<?php
/**
 * RBAC角色管理模型
 * Class RoleModel
 * @author 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Model;
use Think\Model\RelationModel;
class RoleModel extends RelationModel
{
    // 角色表
    protected $tableName = 'Role';

    protected $_link = array(
        'Credits' => array(
            'mapping_type'  => self::MANY_TO_MANY, // 多对多关系
            'relation_table'=> 'php_role_user', // 多对多的中间关联表名称
            'foreign_key'   => 'cid', // 关联的外键名称 外键的默认规则是当前数据对象名称_id
            'relation_foreign_key'=> 'rid',
            // 'mapping_fields'=> 'rname', // 关联要查询的字段
            ),
        );
    //验证
    /*public $validate = array(
        array('rname', 'null', '角色名不能为空', 2, 3),
        array('rname', 'IsRname', '角色已经存在', 2, 3),
    );*/

    /**
     * 添加角色
     */
    public function addRole()
    {
        if ($this->create())
        {
            return $this->add();                
        }
    }

    /**
     * 编辑角色
     * @return [type] [description]
     */
    public function editRole()
    {
        $rid = I('rid', 0, 'intval');
        if ($this->create())
        {
            if ($this->where(array('rid'=> $rid))->save())
            {
                return true;
            }
            else
            {
                $this->error = '会员组没有改动！';
                return false;
            }
        }
    }

    /**
     * 删除角色
     * @param  [type] $rid [description]
     * @return [type]      [description]
     */
    public function delRole()
    {
        $rid = I('rid', 0, 'intval');
        if ($this->where(array('rid'=> $rid))->delete())
        {
            M("user")->where(array('rid' => $rid))->save(array('rid' => 4));
            return true;
        }
        $this->error = '删除失败';
    }

    /*------------------------------属性定义---------------------------------*/

    /**
     * 角色名验证
     * @param [type] $name  [description]
     * @param [type] $value [description]
     * @param [type] $msg   [description]
     * @param [type] $arg   [description]
     */
    public function IsRname($name, $value, $msg, $arg)
    {
        if ($rid = Q('rid', 0, 'intval'))
        {
            $map['rid'] = array('NEQ', $rid);
        }
        $map['rname'] = $value;
        if (M('role')->where($map)->find())
        {
            return $msg;
        }
        return true;
    }
}
