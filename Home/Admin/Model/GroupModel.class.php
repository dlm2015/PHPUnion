<?php
/**
 * RBAC角色管理模型
 * Class RoleModel
 * @author 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Model;
use Think\Model\RelationModel;
class GroupModel extends RelationModel
{
    // 角色表
    protected $tableName = 'Group';

    /**
     * [$_link 关联数据]
     * @var array
     */
    protected $_link = array(
        'Credits'  =>  array(
            'mapping_type'  => self::HAS_ONE,
            'foreign_key'   => 'gid',
            'mapping_key'   => 'gid',
            'mapping_fields'=> 'credits,comment_state,allowsendmessage', // 关联要查询的字段
        ),
    );

    //验证
    protected $_validate = array(
        array('gname', 'require', '会员组名不能为空！', 3, '', 3),
        array('gname', 'IsGname', '会员组名不能为空！', 3, 'function', 3),
    );

    /**
     * 添加会员角色
     */
    public function addGroup()
    {
        // 添加数据处理
        if ($this->create())
        {
            // 添加数据处理
            $gid = $this->add();
            if($gid)
            {
                $data = array(
                    'gid'               => $gid,
                    'credits'      => $_POST['credits'],
                    'comment_state'     => $_POST['comment_state'],
                    'allowsendmessage'  => $_POST['allowsendmessage'],
                    'addtime'           => time(),
                );
                return M('creditslower')->add($data);
            }  
            else
            {
                $this->error = '会员组添加失败！';
                return false;
            }                    
        }
        else
        {
            $this->error = '无法创建数据！';
            return false;
        }
    }

    /**
     * 编辑角色
     * @return [type] [description]
     */
    public function editGroup()
    {
        $gid = I('gid', 0, 'intval');
        if ($this->create())
        {
            if($this->where(array('gid'=> $gid))->save())
            {
                $data['credits'] = $_POST['credits'];
                $data['comment_state'] = $_POST['comment_state'];
                $data['allowsendmessage'] = $_POST['allowsendmessage'];
                return M('creditslower')->where(array('gid'=> $gid))->save($data);
            }
            else
            {
                return $this->error = '会员组更新失败！';
            }
        }
        else
        {
            $this->error = '无法创建数据！';
        }
    }

    /**
     * 删除角色
     * @param  [type] $rid [description]
     * @return [type]      [description]
     */
    public function delGroup()
    {
        $rid = I('rid', 0, 'intval');
        if ($this->where(array('rid'=> $rid))->delete())
        {
            M("user")->where(array('rid' => $rid))->save(array('rid' => 4));
            return true;
        }
        $this->error = '删除失败';
    }

    /*------------------------------属性定义---------------------------------*/

    /**
     * 角色名验证
     * @param [type] $name  [description]
     * @param [type] $value [description]
     * @param [type] $msg   [description]
     * @param [type] $arg   [description]
     */
    public function IsGname($name)
    {
        if (M('group')->where(array('gname'=> $name))->select())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
