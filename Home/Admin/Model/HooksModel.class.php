<?php
/**
 * 插件模型
 * @author 楚羽幽 <Name_Cyu@Foxmai.com>
 * @date    2013-08-14 11:31:21
 */
namespace Admin\Model;
use Think\Model;
class HooksModel extends Model
{
    /**
     * [$_validate 自动验证]
     * @var array
     */
    protected $_validate = array(
        array('name','require','钩子名称必须！'), //默认情况下用正则进行验证
        array('description','require','钩子描述必须！'), //默认情况下用正则进行验证
    );

    /**
     * 自动完成
     * @var array
     */
    protected $_auto = array(
        array('addtime', 'time', 1, 'function'), // 注册时间
    );


    /**
     * [AddHooks 创建钩子]
     */
    public function AddHooks()
    {
        if($this->create())
        {
            return $this->add();
        }
        else
        {
            $this->error = '';
        }
    }


    /**
     * [EditHooks 修改钩子]
     */
    public function EditHooks()
    {
        if($this->create())
        {
            if($this->where(array('id'=> I('id', 0, 'intval')))->save())
            {
                return true;
            }
            else
            {
                $this->error = '钩子信息没有改动！';
                return false;
            }
        }
    }


    /**
     * [DelHooks 删除钩子]
     */
    public function DelHooks()
    {
        $id = I('id', 0, 'intval');
        return $this->where(array('id'=> $id))->delete();
    }
}
