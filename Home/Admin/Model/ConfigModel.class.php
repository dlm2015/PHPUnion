<?php

/**
 *	配置项 - 扩展模型
 *	@author：楚羽幽
 *	QQ 958416459
 */
namespace Admin\Model;
use Think\Model;
class ConfigModel extends Model
{
	// 模型主表
	protected $tableName = 'Config';

    /**
     *  自动验证
     *  @author:楚羽幽
     *  QQ 958416459
     */
    protected $_auto = array(
        // 添加配置项的时间戳
        array('addtime','time',1,'function'),
        );

	// 构造函数
	public function _initialize()
	{
		// 编码设置
    	header('Content-Type:text/html;charset=utf-8');
	}


	//获得配置组
    public function getConfigGroup($isshow = 1)
    {

        return $this->table(C('DB_PREFIX').'config_group')->where(array('isshow'=>$isshow))->order("csort ASC")->select();
    }

	//获得配置组
    public function getConfig($isshow = 1)
    {
        //获得配置组
        $group = $this->table(C('DB_PREFIX').'config_group')->where(array('isshow'=>$isshow))->order("csort ASC")->select();
        foreach ($group as $id => $g)
        {
            $map['cid'] = array('EQ', $g['cid']);
            $config = $this->where($map)->select();
            if ($config)
            {
                foreach ($config as $i => $c)
                {
                    $func = '_' . $c['type'];//text radio select group textarea
                    $config[$i]['_html'] = $this->$func($c);
                    if ($config[$i]['cid'] == $group[$id]['cid'])
            			$group[$id]['_config'][] = $config[$i];
                }
            }
            
        }
        return $group;
    }


	/**
	 *	添加配置项
	 *	@author:楚羽幽
	 *	QQ 958416459
	 */
	public function addConfig()
	{
        // 处理转换参数
        if (!empty($_POST['info']))
        {
            $_POST['info'] = str_replace(' ', '', $_POST['info']);//参数
            $_POST['info'] = \Lib\Util\String::toSemiangle($_POST['info']);//转拼音
        }

        // 验证变量名
        if ($this->where(array('name'=> $_POST['name']))->find())
        {
            $this->error = '变量名已经存在。';
            return false;
        }

        // 添加数据
        if($this->create())
        {
            if($this->add())
            {
                return $this->getFiles();
            }
        }
	}


	/**
     *  修改配置
     *  @author 楚羽幽
     *  @return bool|int
     */
    public function editWebConfig()
    {
        $configData = $_POST['config'];
        if (!is_array($configData))
        {
            $this->error = '数据不能为空';
            return false;
        }
        else
        {
            foreach ($configData as $key => $value)
            {
                $this->where(array('id'=>$key))->save($value);
            }
            return $this->getFiles();
        }
    }


	/**
     * 删除配置
     * @author 楚羽幽
     * @return int
     */
    public function delConfig()
    {
        $id = I('id', 0, 'intval');
        if ($this->delete($id)) {
            return $this->getFiles();
        }
    }



/*--------------------------------配置属性定义-----------------------------------*/


    /**
     *  变量名检测
     *  @author:楚羽幽
     *  E-mail：Name_cyu@foxmail.com
     */
    public function getFindconfig($uname){
        $check = false;
        if($this->where(array('name'=> $uname))->find())
        {
            return $check = ture;
        }
        return $check;
    }

	/**
	 *	配置类型
	 *	@author:楚羽幽
	 *	QQ 958 416 459
	 */
	private function _text($config)
    {
        return "<input type='text' class='input' name='config[{$config['id']}][value]' value='{$config['value']}' />";
    }

    private function _radio($config)
    {
        $info = explode(',', $config['info']);
        $html = '';
        foreach ($info as $radio) {
            $data = explode('|', $radio);//[0]值如1  [1]描述如开启
            $checked = $data[0] == $config['value'] ? ' checked="checked" ' : '';
            $html .= "<label><input type='radio' name='config[{$config['id']}][value]' value='{$data[0]}' $checked/> {$data[1]}</label> ";
        }
        return $html;
    }

    private function _textarea($config)
    {
        return "<textarea class='input' name='config[{$config['id']}][value]'>{$config['value']}</textarea>";
    }

    //列表选项
    private function _select($config)
    {
        $info = explode(',', $config['info']);
        $html = "<select name='config[{$config['id']}][value]' class='input'>";
        foreach ($info as $radio) {
            $data = explode('|', $radio);//[0]值如1  [1]描述如开启
            $selected = $data[0] == $config['value'] ? ' selected="selected" ' : '';
            $html .= "<option value='{$data[0]}' $selected> {$data[1]}</option> ";
        }
        $html .= "</select>";
        return $html;
    }



	/**
	 *	更新配置文件
	 *	@author：楚羽幽
	 *	QQ 958 416 459
	 */
	public function getFiles()
	{
        $configData = $this->select();
        $data = array();
        foreach ($configData as $c) {
            $name = strtoupper($c['name']);
            $data[$name] = $c['value'];
        }

        //写入配置文件
        $content = "<?php\nreturn " . var_export($data, true) . ";\n?>";
        return file_put_contents('Data/Config/config.inc.php', $content);
	}
}