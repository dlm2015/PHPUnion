<?php
/**
 * 视频管理关联模型
 * 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Model;
use Think\Model\RelationModel;
class VideoModel extends RelationModel
{
	// 数据主表
	protected $tableName = 'Video';

	// 私有对象
	private $aid,$cid;

	// 构造函数
	public function _initialize()
	{
		$this->aid = I('aid', 0, 'intval');
		$this->cid = I('cid', 0, 'intval');
	}

	// 自动完成
	public $auto = array(
		// 添加视频自动完成时间
		array('addtime','time','function', 2, 1),
		// 更新视频自动完成时间
		array('updatetime','time','function', 2, 3),
	);

	/**
	 * [$_link 关联数据]
	 * @var array
	 */
	protected $_link = array(
        'User'  =>  array(
            'mapping_type'  => self::HAS_ONE,
            'class_name'    => 'User',
            'foreign_key'   => 'uid',
            'mapping_key'   => 'uid',
            'as_fields'		=> 'username:_author',
        ),
        'Cate'  =>  array(
            'mapping_type'  => self::HAS_ONE,
            'class_name'    => 'Cate',
            'foreign_key'   => 'cid',
            'mapping_key'   => 'cid',
            'as_fields'		=> 'catname:_catname',
        ),
    );

	/**
	 * [createAdd 添加视频]
	 * @return [type] [description]
	 */
	public function CreateAdd()
	{
		if($this->create())
		{
			if($this->Usernick())
			{
				return $this->add();
			}
		}
	}


	/**
	 * [createedit 修改视频]
	 * @return [type] [description]
	 */
	public function CreateEdit()
	{
		if($this->create())
		{
			if($this->Usernick())
			{
				return $this->where(array('aid'=> $this->aid))->save();
			}
			else
			{
				return false;
			}
		}
		else
		{
			$this->error = '无法创建数据！';
		}
	}


	/*--------------------------------------------------属性定义-------------------------------------------------------*/


	// 发布者账户名转id
	public function Usernick()
	{
		// 获取会员ID
		$result = M('user')->where(array('username'=> $this->data['uid']))->find();
		// 重组数组
		return $this->data['uid'] = $result['uid'];
	}



	/**
	 * [Delvideo 删除七牛云储存视频]
	 */
	public function Delvideo()
	{
	}
}