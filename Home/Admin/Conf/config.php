<?php
return array(
	/****************************后台错误提示模板**************************************/
	'VIEW_PATH'				=>	'',	//视图模版路径
	'DEFAULT_THEME'			=>	'',	//默认模板主题
	'THEME_LIST'        	=>  '',//模板主题列表
    
    /****************************后台错误提示模板**************************************/
    'TMPL_ACTION_ERROR'     =>  MODULE_PATH.'View/Public/error.html', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS'   =>  MODULE_PATH.'View/Public/success.html', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE'   =>  MODULE_PATH.'View/Public/exception.html',// 异常页面的模板文件

    /*--------------------------自定义公共标签路径-----------------------------------*/
    'TMPL_PARSE_STRING'		=>	array(
        '__PUBLIC__'        => MODULE_PATH  . 'View/Public',
        '__LIB__'           => __ROOT__.'/Home/Lib',
    ),
);