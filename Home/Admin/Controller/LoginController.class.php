<?php
/**
 *	「PHP联盟」
 *	后台登录控制器
 *	@author:楚羽幽
 *	QQ 958416459
 */
namespace Admin\Controller;
use Think\Controller;
class LoginController extends Controller
{
	/**
	 * [$db 对象定义]
	 * @var [type]
	 */
	private $db;

	/**
	 * [_initialize 构造函数]
	 * @return [type] [description]
	 */
	public function _initialize()
	{
		header('Content-Type:text/html;charset=utf-8');
		$this->db = M('Member');
	}
	/**
	 * [index 登录视图]
	 * @return [type] [description]
	 */
	public function index()
	{
		$this->display();
	}


	/**
	 *	登录验证
	 *	@author：楚羽幽
	 *	QQ958416459
	 */
	public function Login()
	{
		// 得到账户名
		$user = I('username');

		// 判断是否已经登录
		if(isset($_SESSION['uid']) || isset($_SESSION['username']))
		{
			$this->success('你已经登录了！直接跳转后台。',U('Index/index'));
			die;
		}
		
		// 判断用户是否存在，并赋值POST数据
		if(!$username = $this->db->where(array('username'=>$user))->find())
		{
			$this->error('账户不存在！请重新输入。');
		}
		else
		{
			// 判断密码是否正确
			if($username['password'] != md5($_POST['password']))
			{
				$this->error('密码错误！请重新输入。');
			}
			else
			{
				// $verify = $_POST['verify'];
				// 判断验证码是否正确

				/*if(!$this->verify_check($verify))
				{
					$this->error('验证码错误！');
				}*/
				/*else
				{*/
					// 是否是管理员
					/*if($username['role_id'] != 1 || $username['role_id'] != 2)
					{
						$this->error('没有权限进行登录！', U('index'));
					}*/

					// 管理员是否被锁定
					if(!$username['status'])
					{
						// 记录登录信息
						$data = array(
							'uid'			=> $username['uid'],
							'lastip'		=> get_client_ip(),
							'updatetime'	=> time(),
						);

						// 更新登录信息
						$this->db->save($data);

						// SESSION赋值
						session('uid',$username['uid']);
						session('username',$username['username']);
						session('nickname',$username['nickname']);
						session('logintime',date('Y-m-d H:i:s'));
						session('lastip',$username['lastip']);

						// 登录成功！进行跳转。
						$this->success('登录成功！正在跳转后台',U('Index/index'));
					}
					else
					{
						// 检测用户是否被锁定
						$this->error('账户已经被锁定！无法登录');
					}
				// }
			}
		}

	}



	/**
	 *	验证码 - 检测处理
	 *	@author：楚羽幽
	 *	QQ 958 416 459
	 */
	public function verify_check($code, $id = '')
	{
		$verify = new \Think\Verify();
		return $verify->check($code, $id);
	}
	

	/**
 	*	验证码 - 实例化
 	*	@author:楚羽幽
 	*	QQ 958 416 459
 	*/
 	public function verify()
 	{
 		$config = array(
    		'fontSize'    =>    15,		// 验证码字体大小
    		'imageW'      =>	110,	// 验证码宽度
    		'imageH'      =>	35,		// 验证码高度
    		'codeSet'     =>	'0123456789',//验证码字符集
    		'seKey'       =>	'ThinkPHP.CN',		// 验证码密钥
    		'length'      =>    4,     // 验证码位数
    		'useNoise'    =>    false, // 关闭验证码杂点
    		);
 		$Verify =     new \Think\Verify($config);
 		$Verify->entry();
 	}


	/**
	 *	退出处理
	 *	@author：楚羽幽
	 */
	public function logout()
	{
		session(null);
		$this->success('退出成功！跳转登录页面。',U('index'));
	}
}