<?php
/**
 * 附件管理
 * Class FileControl
 * @author 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Controller;
class UploadController extends AuthController
{
    private $db;

    public function _initialize()
    {
        parent::_initialize();
        $this->db = D("Upload");
    }

    /**
     * [index 附件视图列表]
     * @return [type] [description]
     */
    public function index()
    {
        $page = new \Think\Page($this->db->count(), 10);
        $this->page = $page->show();
        $upload = $this->db->Relation(TRUE)->order("id desc")->limit($page->firstRow.','.$page->listRows)->select();
        $this->assign('data', $upload);
        $this->display();
    }

    /**
     * 删除附件
     */
    public function del()
    {
        $id = I("id", null, "intval");
        if ($this->db->delFile($id))
        {
            $this->success("删除成功!");
        }
        $this->error($this->db->error);
    }

    /**
     * 批量删除
     */
    public function batchDel()
    {
        $ids = Q('ids');
        if ($ids && is_array($ids)) {
            foreach ($ids as $id) {
                $this->db->delFile($id);
            }
            $this->success("删除成功!");
        } else {
            $this->error('参数错误');
        }
    }

}
