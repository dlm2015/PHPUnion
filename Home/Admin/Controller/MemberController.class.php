<?php
/**
 * 用户管理控制器
 * @author 楚羽幽 <Name_Cyu@Foxmai.com>
 */
namespace Admin\Controller;
class MemberController extends AuthController
{
    private $db;
    public function _initialize()
    {
        parent::_initialize();
        $this->db = D("MemberRelation");
    }

    /**
     * 用户列表
     * @return [type] [description]
     */
    public function index()
    {
        $count = $this->db->count();
        $page =  new \Think\Page($count,20);
        $pages = $page->show();
        $data = $this->db->relation(TRUE)->limit($page->firstRow.','.$page->listRows)->order("uid ASC")->select();
        // 遍历数组
        foreach ($data as $key => $value)
        {
            foreach ($data[$key]['Role'] as $k => $v)
            {
                $data[$key]['_rname'] .= " ".$v['rname'];
            }
        }
        $this->assign('data', $data);
        $this->assign('page', $pages);
        $this->display();
    }


    /**
     * [add 添加用户]
     */
    public function add()
    {
        if (IS_POST)
        {
            if ($this->db->addUser())
            {
                $this->success("用户添加成功！", U('index'));
            }
            else
            {
                $this->error($this->db->getError());
            }
        }
        else
        {
            $Role = M("Role")->order("rid DESC")->select();
            $Group = M("Group")->order("gid DESC")->select();
            $this->assign('Role', $Role);
            $this->assign('Group', $Group);
            $this->display();
        }
    }

    /**
     * 修改用户
     * @return [type] [description]
     */
    public function edit()
    {
        if (IS_POST)
        {
            if ($this->db->editUser())
            {
                $this->success("用户修改成功", U('index'));
            }
            else
            {
                $this->error($this->db->getError());
            }
        }
        else
        {
            $uid = I("uid", 0, "intval");
            if ($uid)
            {
                $field = $this->db->where(array('uid'=> $uid))->find();
                $Role = M("Role")->order("rid DESC")->select();
                $Group = M("Group")->order("gid DESC")->select();
                $this->assign('Role', $Role);
                $this->assign('Group', $Group);
                $this->assign('field', $field);
                $this->display();
            }
        }
    }

    /**
     * 删除用户
     * @return [type] [description]
     */
    public function del()
    {
        if (IS_POST)
        {
            if ($this->db->delUser())
            {
                $this->success('删除成功');
            }
            else
            {
                $this->error($this->db->getError());
            }
        }
        $this->assign('field', M('user')->find(I('uid')));
        $this->display();
    }


    /*-------------------------------------------------------------属性定义--------------------------------------------------------------------*/

    // 上传头像
    public function avatar()
    {
        $uid = I('uid', 0, 'intval');
        $avatar = $this->db->where(array('uid'=> $uid))->field('avatar')->find();
        p($avatar);exit;
    }
}