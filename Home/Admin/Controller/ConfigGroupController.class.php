<?php
/**
 * 配置组管理
 * Class ConfigGroupController
 * @author 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Controller;
class ConfigGroupController extends AuthController
{
    /**
     * [$db 数据对象]
     * @var [type]
     */
    private $db;

    public function _initialize()
    {
        parent::_initialize();
        $this->db = D('ConfigGroup');
    }

    /**
     * 配置组列表
     */
    public function index()
    {
        //获取组列表
        $data = $this->db->getGroup(1);
        $this->assign('data', $data);
        $this->display();
    }

    /**
     * 添加组
     */
    public function add()
    {
        if (IS_POST)
        {
            if ($this->db->addConfigGroup())
            {
                $this->success('添加成功！', U('index'));
                exit;
            }
            $this->error($this->db->getError());
        }
        $this->display();
    }

    /**
     * @return [type] [修改配置组]
     */
    public function edit()
    {
        if (IS_POST)
        {
            if ($this->db->editConfigGroup())
            {
                $this->success('修改成功', U('index'));
                exit;
            }
            $this->error($this->db->getError());
        }
        else
        {
            $cid = I('cid', 0, 'intval');
            $field = $this->db->find($cid);
            if (!$field)
            {
                $this->error($this->db->getError);
            }
            $this->assign("field", $field);
            $this->display();
        }
    }

    /**
     * 删除配置组
     */
    public function del()
    {
        if ($this->db->delConfigGroup())
        {
            $this->success('删除成功！', U('index'));
            exit;
        }
        $this->error($this->db->getError());
    }
}