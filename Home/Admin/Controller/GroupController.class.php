<?php
/**
 * 「PHP联盟」
 * 用户等级控制器
 * 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Controller;
class GroupController extends AuthController
{
	private $db;

	public function _initialize()
	{
		header('content-type:text/html;charset=utf-8');
		$this->db = D('Group');
	}
	/**
	 * [index 显示等级组]
	 * @return [type] [description]
	 */
	public function index()
	{
		$data = $this->db->Relation(TRUE)->select();
		$this->assign('data', $data);
		$this->display();
	}


	/**
	 * [add 添加用户等级组]
	 */
	public function add()
	{
		if (IS_POST)
        {
            if ($this->db->addGroup())
            {
                $this->success('添加成功', U('index'));
                exit;
            }
            $this->error($this->db->getError);
        }
        $this->display();
	}


	/**
	 * [edit 修改用户等级组]
	 * @return [type] [description]
	 */
	public function edit()
	{
		if(IS_POST)
		{
			if($this->db->editGroup())
			{
				$this->success('操作成功！', U('index'));
			}
			else
			{
				$this->error($this->db->getError());
			}
		}
		else
		{
			$gid = I('gid', 0, 'intval');
			$data = $this->db->relation(true)->where(array('gid'=> $gid))->find();
        	$this->assign('field', $data);
			$this->display();
		}
	}


	/**
	 * [del 删除等级组]
	 * @return [type] [description]
	 */
	public function del()
	{
		if($this->db->delGroup())
		{
			$this->success('操作成功！');
		}
		else
		{
			$this->error($this->db->getError());
		}
	}
}