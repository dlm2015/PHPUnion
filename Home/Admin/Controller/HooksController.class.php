<?php
/**
 * 钩子管理控制器
 * 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Controller;
class HooksController extends AuthController
{
	// 私有对象
	private $db;

	/**
	 * [_initialize 构造函数]
	 * @return [type] [description]
	 */
	public function _initialize()
	{
		parent::_initialize();
		$this->db = D('Hooks');
	}


	/**
	 * [index 钩子列表]
	 * @return [type] [description]
	 */
	public function index()
	{
		$result = $this->db->select();
		$this->assign('data', $result);
        $this->display();
    }


	/**
	 * [add 添加钩子]
	 */
	public function create()
	{
		if(IS_POST)
		{
			if($this->db->AddHooks())
			{
				$this->success('操作成功！', U('index'));
			}
			else
			{
				$this->error($this->db->getError());
			}
		}
		else
		{
			$this->display();
		}
	}


	/**
	 * [edit 修改钩子]
	 * @return [type] [description]
	 */
	public function edit()
	{
		if(IS_POST)
		{
			if($this->db->EditHooks())
			{
				$this->success('操作成功！', U('index'));
			}
			else
			{
				$this->error($this->db->getError());
			}
		}
		else
		{
			$result = $this->db->where(array('id'=> I('id', 0, 'intval')))->find();
			$this->assign('field', $result);
			$this->display();
		}
	}


	/**
	 * [del 删除钩子]
	 * @return [type] [description]
	 */
	public function del()
	{
		if($this->db->DelHooks())
		{
			$this->success('删除钩子成功！', U('index'));
		}
		else
		{
			$this->error($this->db->getError());
		}
	}

	/*--------------------------------------属性定义--------------------------------------------*/
}