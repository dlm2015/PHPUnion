<?php
/**
 *	配置项控制器
 *	@author：楚羽幽
 *	QQ：958416459
 */
namespace Admin\Controller;
use Think\Controller;
class ConfigController extends AuthController
{
	// 模型对象
	private $db;

	// 构造函数
	public function _initialize()
	{
		parent::_initialize();
		$this->db = D('Config');
	}

	//添加配置项
	public function add()
    {
        if (IS_POST)
        {
            // 检测变量名
            if($this->db->getFindconfig($_POST['name']))
            {
                $this->error('参数名名已存在，请更换参数名！',U('add'));
            }

            // 添加配置项
            if ($this->db->addConfig())
            {
                $this->success('添加成功!', U('webConfig'));
            }
            else
            {
                $this->error($this->db->getError());
            }
        }
        else
        {
            $configGroup = $this->db->getConfigGroup();
            $this->assign("configGroup", $configGroup);
            $this->display();
        }
    }


	//修改网站配置(基本配置）
    public function webConfig()
    {
        if (IS_POST)
        {
            if($this->db->editWebConfig())
            {
                $this->success("网站信息更新成功！", U('webConfig'));
            }
            else
            {
                $this->error($this->db->getError());
            }
        }
        else
        {
            // 分配配置组
            $data = $this->db->getConfig();
            $this->assign('data', $data);
            $this->display();
        }
    }


	//删除配置
    public function del()
    {
        if ($this->db->delConfig())
        {
            $this->success('操作成功');
        }
        else
        {
            $this->error($this->db->error());
        }
    }


    //更新缓存
    public function updateCache()
    {
        if ($this->db->getFiles())
        {
            $this->success('配置项缓存更新成功！');
        }
        else
        {
            $this->error($this->db->getError());
        }
    }
}