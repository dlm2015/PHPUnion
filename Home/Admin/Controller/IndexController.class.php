<?php
/**
 * 后台首页控制器
 */
namespace Admin\Controller;
use Think\Controller;
class IndexController extends AuthController
{
    public function index()
    {
        $this->display();
    }
}