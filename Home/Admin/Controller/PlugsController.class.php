<?php
/**
 * 扩展后台管理页面
 * @author 楚羽幽 <Name_Cyu@Foxmail.com>
 */
namespace Admin\Controller;
class PlugsController extends AuthController
{
    private $db;
    /**
     * [_initialize 构造函数]
     * @return [type] [description]
     */
    public function _initialize()
    {
        parent::_initialize();

        //删除插件缓存
        S('hooks',null);

        $this->db = D('Plugs');
        $this->assign('field',array('install'=> '已安装插件', 'plugs' => D('Plugs')->getAdminList()));
    }



    /**
     * [index 插件列表]
     * @return [type] [description]
     */
    public function index()
    {
        $list       =   $this->db->getList();
        $request    =   (array)I('request.');
        $total      =   $list? count($list) : 1 ;
        $listRows   =   10 > 0 ? 10 : 10;
        $page       =   new \Think\Page($total, $listRows, $request);
        $voList     =   array_slice($list, $page->firstRow, $page->listRows);
        $p          =   $page->show();
        $this->assign('_list', $voList);
        $this->assign('_page', $p ? $p: '');
        $this->display();
    }


    /**
     * [create 创建插件]
     * @return [type] [description]
     */
    public function create()
    {
        if (IS_POST)
        {
            // 数据处理开始
            if($this->db->create())
            {
                // 获取数据
                $data = $_POST;
                // 插件名首字母大小
                $data['name']   =   ucfirst($data['name']);
                // 插件代码预览
                $addonFile      =   $this->preview(false);
                // 插件目录路径
                $addons_dir     =   UNION_ADDON_PATH;

                // 验证插件标识
                if($this->PlugsCheck($data['name']))
                {
                    $this->error('操作错误，该插件已经存在！', U('index'));
                    continue;
                }

                // 创建目录结构
                $files          =   array();
                $addon_dir      =   "$addons_dir{$data['name']}/";
                $files[]        =   $addon_dir;
                $addon_name     =   "{$data['name']}Addon.class.php";
                $files[]        =   "{$addon_dir}{$addon_name}";

                // 开启外部访问，创建模型与控制器目录及文件
                if($data['has_outurl'])
                {
                    $files[]    =   "{$addon_dir}Controller/";
                    $files[]    =   "{$addon_dir}Controller/{$data['name']}Controller.class.php";
                    $files[]    =   "{$addon_dir}Model/";
                    $files[]    =   "{$addon_dir}Model/{$data['name']}Model.class.php";
                }

                // 使用函数创建目录及文件
                create_dir_or_files($files);

                //-------------------配置项文件----------------------
                if ($data['config'] == 1)
                {
                    copy(MODULE_PATH . 'Data/Addon/configAddon.php', $addon_dir . 'config.php');
                }

                // 写文件
                file_put_contents("{$addon_dir}{$addon_name}", $addonFile);
                if($data['has_outurl'])
                {
                    $addonController = <<<str
<?php
/**
 * 「PHP联盟」
 * {$data['name']} 控制器
 * {$data['author']} <{$data['email']}>
 */
namespace Addons\\{$data['name']}\Controller;
use Home\Controller\AddonsController;
class {$data['name']}Controller extends AddonsController
{
    // 方法
}
str;
                file_put_contents("{$addon_dir}Controller/{$data['name']}Controller.class.php", $addonController);
                $addonModel = <<<str
<?php
/**
 * 「PHP联盟」
 * {$data['name']} 模型
 * {$data['author']} <{$data['email']}>
 */
namespace Addons\\{$data['name']}\Model;
use Think\Model;
class {$data['name']}Model extends Model
{
    // 方法
}
str;
                file_put_contents("{$addon_dir}Model/{$data['name']}Model.class.php", $addonModel);
            }
            $this->success('恭喜，插件创建成功！',U('index'));

            // 数据处理结束
            }
            else
            {
                $this->error($this->db->getError());
            }         
        }
        else
        {
            $this->assign('Hooks', M('Hooks')->select());
            $this->display();
        }
    }


    /*----------------------------------------属性定义----------------------------------------------*/

    // 验证插件唯一性
    public function PlugsCheck($name, $arr)
    {
        if (is_dir(UNION_ADDON_PATH . $name))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    /**
     * [preview 插件控制器代码]
     * @param  boolean $output [description]
     * @return [type]          [description]
     */
    public function preview($output = true)
    {
        // 获取数据
        $data                   =   $_POST;
        // 插件名首字母大小
        $data['name']           =   ucfirst($data['name']);
        $data['status']         =   (int)$data['status'];
        $extend                 =   array();

        if($data['has_adminlist'] == 1)
        {
            $admin_list = <<<str


        public \$admin_list = array(
            'model'     =>  'Example',      // 要查的表
            'fields'    =>  '*',            // 要查的字段
            'map'       =>  '',             // 查询条件, 如果需要可以再插件类的构造方法里动态重置这个属性
            'order'     =>  'id desc',      // 排序,
            'listKey'   =>  array(
                // 这里定义的是除了id序号外的表格里字段显示的表头名
                '字段名'    => '表头显示名'
            ),
        );
str;
        $extend[] = $admin_list;
    }
        $extend = implode('', $extend);
        $hooks = '';
        foreach ($data['hooks'] as $value)
        {
            $hooks .= <<<str
        // 实现的{$value}钩子方法
        public function {$value}(\$param)
        {
            // 钩子行为方法
        }

str;
        }

        $tpl = <<<str
<?php
/**
 * {$data['title']}插件
 * @author {$data['author']}
 */
namespace Addons\\{$data['name']};
use Common\Controller\Addon;
class {$data['name']}Addon extends Addon
{
        public \$info = array(
            'name'          =>  '{$data['name']}',
            'title'         =>  '{$data['title']}',
            'description'   =>  '{$data['description']}',
            'status'        =>  {$data['status']},
            'author'        =>  '{$data['author']}',
            'email'         =>  '{$data['email']}',
            'version'       =>  '{$data['version']}'
        );{$extend}

        // 安装
        public function install()
        {
            return true;
        }

        // 卸载
        public function uninstall()
        {
            return true;
        }

{$hooks}
}
str;
        if($output)
            exit($tpl);
        else
            return $tpl;
    }
}
