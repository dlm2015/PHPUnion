<?php
/**
 * 更新缓存控制器
 * @author 楚羽幽 <Name_Cyu@Foxmai.com>
 */
namespace Admin\Controller;
class CacheController extends AuthController
{
    /**
     * [index 缓存视图]
     * @return [type] [description]
     */
    public function index()
    {
        if (IS_POST)
        {
            is_file(RUNTIME_PATH . 'common~runtime.php') && unlink(RUNTIME_PATH . 'common~runtime.php');
            \Lib\Util\Dir::del('Temp/Compile');
            \Lib\Util\Dir::del('Temp/Content');
            \Lib\Util\Dir::del('Temp/Table');
            // 缓存更新动作
            S('updateCacheAction', $_POST['Action']);
            $this->success('正在准备进行更新全站缓存...', U('updateCache', array('action' => 'Config')), 1);
        }
        else
        {
            $this->display();
        }
    }

    /**
     * [updateCache 更新缓存]
     * @return [type] [description]
     */
    public function updateCache()
    {
        $actionCache = S('updateCacheAction');
        if ($actionCache)
        {
            while ($action = array_shift($actionCache))
            {
                switch ($action)
                {
                    case "Config" :
                        $Model = D("Config");
                        $Model->updateCache();
                        S('updateCacheAction',$actionCache);
                        $this->success('网站配置更新完毕...', U('updateCache'), 1);
                    break;
                    case "Cate" :
                        $Model = D('Cate');
                        $Model->updateCache();
                        S('updateCacheAction',$actionCache);
                        $this->success('频道栏目更新完毕...', U('updateCache'), 1);
                    break;
                    case "Addons" :
                        $Model = D('Addons');
                        $Model->updateAddonCache();
                        S('updateCacheAction',$actionCache);
                        $this->success('插件缓存更新完毕...', U('updateCache'), 1);
                    break;
                }
            }
            go('updateCache');
        }
        else
        {
            S('updateCacheAction', null);
            $this->success('全站缓存更新成功...', U('index'));
        }
    }
}
