<?php
/**
 * 前台首页控制器
 */
namespace Home\Controller;
class IndexController extends CommonController
{
	/**
	 * [__init 构造函数]
	 * @return [type] [description]
	 */
	public function _initialize()
    {
        // 网站关闭检测
        // $this->CheckWebClose();
        // 缓存目录
        // $this->cacheDir = TEMP_PATH . 'Content/';
    }
    /**
     * [index 首页视图]
     * @return [type] [description]
     */
    public function index()
    {
        $this->display($this->VIEW_DIR . '/' . ACTION_NAME);
    }
}
