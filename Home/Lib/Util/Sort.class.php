<?php
/**
 *	权限节点排序栏目
 *	「PHP联盟」
 *	@author：楚羽幽
 *	QQ958416459
 */
namespace Lib\Util;
class Sort
{
	static public $SortList = array(); // 存放无限级分类结果
	public function create($data, $pid = 0)
	{
		foreach($data as $key => $value)
		{
			if($value['pid'] == $pid)
			{
				self::$SortList[] = $value;
				unset($data[$key]);
				self::create($data, $value['id']);
			}
		}
		return self::$SortList;
	}
}