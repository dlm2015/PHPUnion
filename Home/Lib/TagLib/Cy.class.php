<?php
/**
 * 自定义扩展标签库
 * PHP联盟
 * @author 楚羽幽
 */
namespace Lib\TagLib;
use Think\Template\TagLib;
class Cy extends TagLib
{

    // 标签定义
    protected $tags   =  array(
        'Pintuer' =>  array('attr'=>'','close'=>0),
        );

    /**
     * Bootstrap解析标签
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return active
     */
    public function _Pintuer($tag,$content) {
        return "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n<script type=\"text/javascript\" src=\"__LIB__/Pintuer/jquery-1.11.0.js\"></script>\n<link rel=\"stylesheet\" type=\"text/css\" href=\"__LIB__/Pintuer/pintuer.css\" />\n<script type=\"text/javascript\" src=\"__LIB__/Pintuer/pintuer.js\"></script>\n<script type=\"text/javascript\" src=\"__LIB__/Pintuer/respond.js\"></script>";
    }
}